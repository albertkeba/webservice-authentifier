<form class="js-validation-login form-horizontal push-30-t" action="{{ route('authentifier.login') }}" method="post">
    @csrf

    <div class="form-group{{ $errors->has('registration') ? ' has-error' : '' }}">
        <div class="col-xs-12">
            <div class="form-material form-material-primary floating">
                <input class="form-control" type="text" id="registration" name="registration" value="{{ old('registration') }}" required autofocus />
                <label for="registration">{{ __('login.login') }}</label>
            </div>
            @if ($errors->has('registration'))
                <div class="help-block text-right animated fadeInDown">
                    <span>{{ $errors->first('registration') }}</span>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="col-xs-12">
            <div class="form-material form-material-primary floating">
                <input class="form-control" type="password" id="password" name="password" required />
                <label for="password">{{ __('login.password.label') }}</label>
            </div>

            @if ($errors->has('password'))
                <div class="help-block text-right animated fadeInDown">
                    <span>{{ $errors->first('password') }}</span>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group push-30-t">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <button class="btn btn-sm btn-block btn-primary" type="submit">{{ __('login.login') }}</button>
        </div>
    </div>
</form>
