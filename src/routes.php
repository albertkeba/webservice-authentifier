<?php
Route::group(['middleware'=>['web']], function() {
    Route::get('authentifier', 'Authentifier\App\Http\Controllers\AuthentifierController@index');
    Route::post('authentifier/login', 'Authentifier\App\Http\Controllers\AuthentifierController@login')->name('authentifier.login');
});
