<?php

namespace Authentifier;

use Illuminate\Support\ServiceProvider;

class AuthentifierServiceProvider extends ServiceProvider {
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';
        // echo __DIR__ . '/routes.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Aninf\Authentifier\App\Http\Controllers\AuthentifierController');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'authentifier');
        // $this->app->register('Aninf\SoapWrapper\App\Providers\SoapWrapperServiceProvider');
        // App::register('Aninf\SoapWrapper\App\Providers\SoapWrapperServiceProvider');
    }
}
